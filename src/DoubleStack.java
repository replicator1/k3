import java.util.*;

public class DoubleStack {

   public static void main (String[] argum) {
      String s = "2 5 SWAP5 SWAP -";
      System.out.println(interpret(s));

      //s = null;
      //System.out.println(interpret(s));

      //s = "3 4 + - 5";
      //System.out.println(interpret(s));

      //s = "35. 10. -3. + / 2.";

      // System.out.println(interpret(s));

   }

   private List<Double> doubleStack;

   private static final List<String> operators = Arrays.asList("+", "-", "*", "/", "ROT", "SWAP", "DUP");

   DoubleStack() {
      doubleStack = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {

      DoubleStack tmp = new DoubleStack();
      for (Double aDouble : doubleStack) {
         tmp.push(aDouble);
      }
      return tmp;
   }

   public boolean stEmpty() {
      return doubleStack.size() == 0;
   }

   public void push(double a) {
      doubleStack.add(a);
   }

   public double pop() {
      if (stEmpty()) {
         throw new IndexOutOfBoundsException("Stack underflow, not enough elements");
      }
      Double tmp = doubleStack.get(doubleStack.size() - 1);

      doubleStack.remove(doubleStack.size() - 1);
      return tmp;
   }

   public void op(String s) {

      if (stEmpty()) {
         throw new IndexOutOfBoundsException("Stack underflow, not enough elements." +
                 " Operator: " + s + " cannot be applied");
      }

      switch (s) {
         case "DUP":
            if (doubleStack.size() < 1) {
               throw new IndexOutOfBoundsException("Stack underflow, not enough elements." +
                       " Operator: " + s + " cannot be applied");
            }
            break;
         case "ROT":
            if (doubleStack.size() < 3) {
               throw new IndexOutOfBoundsException("Stack underflow, not enough elements." +
                       " Operator: " + s + " cannot be applied");
            }
            break;
         default:
            if (doubleStack.size() < 2) {
               throw new IndexOutOfBoundsException("Stack underflow, not enough elements." +
                       " Operator: " + s + " cannot be applied");
            }
      }


      if (!operators.contains(s)) {
         throw new IllegalArgumentException("Illegal argument: " + s);
      }

      double x = 0;
      double y = 0;

      if (Objects.equals(s, "DUP")) {
         x = pop();
      } else {
         x = pop();
         y = pop();
      }



      switch (s) {
         case "+":
            push(x + y);
            break;

         case "-":
            push(y - x);
            break;

         case "*":
            push(x * y);
            break;

         case "/":
            push(y / x);
            break;

         case "SWAP":
            push(x);
            push(y);
            break;

         case "ROT":
            double z = pop();
            push(y);
            push(x);
            push(z);
            break;

         case "DUP":
            push(x);
            push(x);

            default:
            break;
      }
   }

   public double tos() {
      if (stEmpty()) {
         throw new IndexOutOfBoundsException("Stack underflow, not enough elements");
      }
      return doubleStack.get(doubleStack.size() - 1);
   }

   @Override
   public boolean equals (Object o) {

      if (!(o.getClass().equals(this.getClass()))) {
         return false;
      }

      if ( ((DoubleStack)o).doubleStack.size() != doubleStack.size() ) {
         return false;
      }

      for (int i = 0; i <= doubleStack.size() - 1; i++) {
         if (!(((DoubleStack)o).doubleStack.get(i).equals(doubleStack.get(i)))) {
            return false;
         }
      }
      return true;
   }

   @Override
   public String toString() {
      StringBuilder result = new StringBuilder();

      for (Double aDouble : doubleStack) {
         result.append(aDouble);
      }

      return result.toString();
   }

   // https://www.geeksforgeeks.org/evaluate-the-value-of-an-arithmetic-expression-in-reverse-polish-notation-in-java/
   public static double interpret (String pol) {

      if (pol == null) {
         throw new IllegalArgumentException("Illegal expression: " + pol + ", expression cannot be null");
      }

      List<String> expression = splitString(pol);

      if (expression.size() == 0) {
         throw new IllegalArgumentException("Illegal expression: " + pol + ", expression cannot be empty");
      }

      String choice;
      DoubleStack db = new DoubleStack();

      for (String element : expression) {
         if (!operators.contains(element)) {

            // check if element is valid
            try {
               double num = Double.parseDouble(element);
               db.push(num);
               continue;

            } catch (NumberFormatException e) {
               throw new NumberFormatException("Illegal argument: " +  e + ", in expression: " + pol);
            }

         } else {
            choice = element;
         }
         db.op(choice);
      }

      if (db.doubleStack.size() != 1) {
         throw new IndexOutOfBoundsException("Illegal expression:" + pol + ", not enough elements");
      }
      return db.pop();
   }

   private static List<String> splitString(String str) {
      String[] strSplit = str.split("\\s+");
      List<String> result =new ArrayList<>();

      for (String s : strSplit) {
         if (!s.isEmpty()) {
            result.add(s);
         }
      }
      return result;
   }
}